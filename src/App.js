import React from 'react';
import logo from './logo.svg';
import './App.css';
import NeedHelp from './components/NeedHelp';
import NavbarMain from './components/navbar/NavbarMain';

function App() {
  return (
    <div className="App">
      <NavbarMain />
      <NeedHelp />
    </div>
  );
}

export default App;
